# NullBank - Equipe 374876

* Francisco Mauro Falcão Matias Filho - 374876
* Maria Raquel Lopes de Couto - 374889
* Francisco Thales Rocha Sousa - 378587

## Ferramentas Utilizadas

* [MySQL Workbench](https://www.mysql.com/products/workbench/) Versão 6.3.9 Comunity.
* [MySQL Comunity Server](https://dev.mysql.com/downloads/mysql/) Versão 5.7.19.
* [NodeJS](https://nodejs.org/en/) Versão 6.11.5
* [Electron](https://electron.atom.io/) Versão 1.7.9 - Criador de aplicações Desktop multiplataforma com JavaScript, HTML e CSS.
* [NPM MySQL](https://www.npmjs.com/package/mysql) Versão 2.15.0 - Conector MySQL para JavaScript.
* [Bootstrap](https://getbootstrap.com/) Versão 4.0 beta - Estilizador de HTML

## Execução por linha de Comando

Ao baixar, executar os seguintes comandos na pasta do repositório:

`npm install`

`npm install electron`

`npm install mysql`

`npm install object-compare`

Para executar:

`npm start`

## Geração de Executável e Instalador
Para gerar o executável e instalador:

`npm run package-win`

`node installers/windows/createinstaller.js`

### O arquivo de instalação se encontra na pasta release-builds\windows-installer\NullBank-374876.exe